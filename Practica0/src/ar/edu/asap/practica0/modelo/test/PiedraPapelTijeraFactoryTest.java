package ar.edu.asap.practica0.modelo.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.asap.practica0.modelo.Papel;
import ar.edu.asap.practica0.modelo.Piedra;
import ar.edu.asap.practica0.modelo.PiedraPapelTijeraFactory;
import ar.edu.asap.practica0.modelo.Tijera;



class PiedraPapelTijeraFactoryTest {
	// lote de pruebas
	Piedra piedra;
	Papel papel;
	Tijera tijera;
	

	@BeforeEach
	// se ejecuta antes del test
	void setUp() throws Exception {
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
		
	}

	@AfterEach
	// se ejecuta despues de cada test
	void tearDown() throws Exception {
		piedra = null;
		papel = null;
		tijera = null;
		
	}
	
	// comparaciones Piedra
	@Test 
	void testPiedraGanaATijera_comparar() {
		assertEquals(1, piedra.comparar(tijera));
	}
	
	@Test 
	void testPiedraPierdeConPapel_comparar() {
		assertEquals(-1, piedra.comparar(papel));
	}
	
	@Test 
	void testPiedraEmpataConPiedra_comparar() {
		assertEquals(0, piedra.comparar(piedra));
	}
	
	// comparaciones Papel
	@Test 
	void testPapelGanaAPiedra_comparar() {
		assertEquals(1, papel.comparar(piedra));
	}
	
	@Test 
	void testPapelPierdeConTijera_comparar() {
		assertEquals(-1, papel.comparar(tijera));
	}
	
	@Test 
	void testPapelEmpataConPapel_comparar() {
		assertEquals(0, papel.comparar(papel));
	}
	
	// comparaciones Tijera
	
	@Test 
	void testTijeraGanaAPapel_comparar() {
		assertEquals(1, tijera.comparar(papel));
	}
	
	@Test 
	void testTijeraPierdeConPiedra_comparar() {
		assertEquals(-1, tijera.comparar(piedra));
	}
	
	@Test 
	void testTijeraEmpataConTijera_comparar() {
		assertEquals(0, tijera.comparar(tijera));
	}
	
	// comparar Piedra
	@Test 
	void testPiedraGanaATijera_comparar_DescripcionResultado() {
		int result= piedra.comparar(tijera);
		assertEquals("piedra le gana a tijera", piedra.getDescripcionResultado());
	}
	
	@Test 
	void testPiedraEmpataConPiedra_comparar_DescripcionResultado() {
		int result= piedra.comparar(piedra);
		assertEquals("piedra empata piedra", piedra.getDescripcionResultado());
	}
	
	@Test 
	void testPiedraPierdeConPapel_comparar_DescripcionResultado() {
		int result= piedra.comparar(papel);
		assertEquals("piedra pierde con papel", piedra.getDescripcionResultado());
	}
	
	// comparar Papel
	@Test 
	void testPapelGanaAPiedra_comparar_DescripcionResultado() {
		int result= papel.comparar(piedra);
		assertEquals("papel le gana a piedra", papel.getDescripcionResultado());
	}
	
	@Test 
	void testPapelEmpataConPapel_comparar_DescripcionResultado() {
		int result= papel.comparar(papel);
		assertEquals("papel empata papel", papel.getDescripcionResultado());
	}
	
	@Test 
	void testPapelPierdeConTijera_comparar_DescripcionResultado() {
		int result= papel.comparar(tijera);
		assertEquals("papel pierde con tijera", papel.getDescripcionResultado());
	}
	
	// comparar Tijera
	
	@Test 
	void testTijeraGanaAPapel_comparar_DescripcionResultado() {
		int result= tijera.comparar(papel);
		assertEquals("tijera le gana a papel", tijera.getDescripcionResultado());
	}
	
	@Test 
	void testTijeraEmpataConTijera_comparar_DescripcionResultado() {
		int result= tijera.comparar(tijera);
		assertEquals("tijera empata tijera", tijera.getDescripcionResultado());
	}
	
	@Test 
	void testTijeraPierdeConPiedra_comparar_DescripcionResultado() {
		int result= tijera.comparar(piedra);
		assertEquals("tijera pierde con piedra", tijera.getDescripcionResultado());
	}
	
	
	
	
	@Test
	void testGetInstance_tijera() {
		assertEquals("tijera", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.TIJERA)
				.getNombre());

	}
	
	
	@Test
	void testGetInstance_piedra() {
		assertEquals("piedra", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA)
				.getNombre());

	}
	
	
	@Test
	void testGetInstance_papel() {
		assertEquals("papel", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PAPEL)
				.getNombre());
	}
	

}
