package ar.edu.asap.practica0.pptlspock.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.asap.practica0.pptlspock.Papel;
import ar.edu.asap.practica0.pptlspock.Piedra;
import ar.edu.asap.practica0.pptlspock.PiedraPapelTijeraLagartoSpockFactory;
import ar.edu.asap.practica0.pptlspock.Tijera;
import ar.edu.asap.practica0.pptlspock.Lagarto;
import ar.edu.asap.practica0.pptlspock.Spock;



class PiedraPapelTijeraLagartoSpockFactoryTest {
	// lote de pruebas
	Piedra piedra;
	Papel papel;
	Tijera tijera;
	Lagarto lagarto;
	Spock spock;
	
	

	@BeforeEach
	// se ejecuta antes del test
	void setUp() throws Exception {
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
		lagarto = new Lagarto();
		spock = new Spock();
		
	}

	@AfterEach
	// se ejecuta despues de cada test
	void tearDown() throws Exception {
		piedra = null;
		papel = null;
		tijera = null;
		lagarto = null;
		spock = null;
		
	}
	
	// comparaciones Piedra
	@Test 
	void testPiedraGanaATijera_comparar() {
		assertEquals(1, piedra.comparar(tijera));
	}
	
	@Test 
	void testPiedraGanaALagarto_comparar() {
		assertEquals(1, piedra.comparar(lagarto));
	}
	
	@Test 
	void testPiedraPierdeConPapel_comparar() {
		assertEquals(-1, piedra.comparar(papel));
	}
	
	@Test 
	void testPiedraPierdeConSpock_comparar() {
		assertEquals(-1, piedra.comparar(spock));
	}
	
	@Test 
	void testPiedraEmpataConPiedra_comparar() {
		assertEquals(0, piedra.comparar(piedra));
	}
	
	// comparaciones Papel
	@Test 
	void testPapelGanaAPiedra_comparar() {
		assertEquals(1, papel.comparar(piedra));
	}
	
	@Test 
	void testPapelGanaASpock_comparar() {
		assertEquals(1, papel.comparar(spock));
	}
	
	@Test 
	void testPapelPierdeConTijera_comparar() {
		assertEquals(-1, papel.comparar(tijera));
	}
	
	@Test 
	void testPapelPierdeConLagarto_comparar() {
		assertEquals(-1, papel.comparar(lagarto));
	}
	
	@Test 
	void testPapelEmpataConPapel_comparar() {
		assertEquals(0, papel.comparar(papel));
	}
	
	// comparaciones Tijera
	
	@Test 
	void testTijeraGanaAPapel_comparar() {
		assertEquals(1, tijera.comparar(papel));
	}
	
	@Test 
	void testTijeraGanaALagarto_comparar() {
		assertEquals(1, tijera.comparar(lagarto));
	}
	
	@Test 
	void testTijeraPierdeConPiedra_comparar() {
		assertEquals(-1, tijera.comparar(piedra));
	}
	
	@Test 
	void testTijeraPierdeConSpock_comparar() {
		assertEquals(-1, tijera.comparar(spock));
	}
	
	@Test 
	void testTijeraEmpataConTijera_comparar() {
		assertEquals(0, tijera.comparar(tijera));
	}

	// comparaciones Lagarto
	
	@Test 
	void testLagartoGanaAPapel_comparar() {
		assertEquals(1, lagarto.comparar(papel));
	}
	
	@Test 
	void testLagartoGanaASpock_comparar() {
		assertEquals(1, lagarto.comparar(spock));
	}
	
	@Test 
	void testLagartoPierdeConPiedra_comparar() {
		assertEquals(-1, lagarto.comparar(piedra));
	}
	
	@Test 
	void testLagartoPierdeConTijera_comparar() {
		assertEquals(-1, lagarto.comparar(tijera));
	}
	
	@Test 
	void testLagartoEmpataConLagarto_comparar() {
		assertEquals(0, lagarto.comparar(lagarto));
	}
	
	// comparaciones Spock
	
	@Test 
	void testSpockGanaATijera_comparar() {
		assertEquals(1, spock.comparar(tijera));
	}
	
	@Test 
	void testSpockGanaAPiedra_comparar() {
		assertEquals(1, spock.comparar(piedra));
	}
	
	@Test 
	void testSpockPierdeConPapel_comparar() {
		assertEquals(-1, spock.comparar(papel));
	}
	
	@Test 
	void testSpockPierdeConLagarto_comparar() {
		assertEquals(-1, spock.comparar(lagarto));
	}
	
	@Test 
	void testSpockEmpataConSpock_comparar() {
		assertEquals(0, spock.comparar(spock));
	}
	
	
	// comparar Piedra
	@Test 
	void testPiedraGanaATijera_comparar_DescripcionResultado() {
		int result= piedra.comparar(tijera);
		assertEquals("piedra le gana a tijera", piedra.getDescripcionResultado());
	}
	
	@Test 
	void testPiedraGanaALagarto_comparar_DescripcionResultado() {
		int result= piedra.comparar(lagarto);
		assertEquals("piedra le gana a lagarto", piedra.getDescripcionResultado());
	}
	
	@Test 
	void testPiedraEmpataConPiedra_comparar_DescripcionResultado() {
		int result= piedra.comparar(piedra);
		assertEquals("piedra empata piedra", piedra.getDescripcionResultado());
	}
	
	@Test 
	void testPiedraPierdeConPapel_comparar_DescripcionResultado() {
		int result= piedra.comparar(papel);
		assertEquals("piedra pierde con papel", piedra.getDescripcionResultado());
	}
	
	@Test 
	void testPiedraPierdeConSpock_comparar_DescripcionResultado() {
		int result= piedra.comparar(spock);
		assertEquals("piedra pierde con spock", piedra.getDescripcionResultado());
	}
	
	
	// comparar Papel
	@Test 
	void testPapelGanaAPiedra_comparar_DescripcionResultado() {
		int result= papel.comparar(piedra);
		assertEquals("papel le gana a piedra", papel.getDescripcionResultado());
	}
	
	@Test 
	void testPapelGanaASpock_comparar_DescripcionResultado() {
		int result= papel.comparar(spock);
		assertEquals("papel le gana a spock", papel.getDescripcionResultado());
	}
	
	@Test 
	void testPapelEmpataConPapel_comparar_DescripcionResultado() {
		int result= papel.comparar(papel);
		assertEquals("papel empata papel", papel.getDescripcionResultado());
	}
	
	@Test 
	void testPapelPierdeConTijera_comparar_DescripcionResultado() {
		int result= papel.comparar(tijera);
		assertEquals("papel pierde con tijera", papel.getDescripcionResultado());
	}
	
	@Test 
	void testPapelPierdeConLagarto_comparar_DescripcionResultado() {
		int result= papel.comparar(lagarto);
		assertEquals("papel pierde con lagarto", papel.getDescripcionResultado());
	}
	
	
	// comparar Tijera
	
	@Test 
	void testTijeraGanaAPapel_comparar_DescripcionResultado() {
		int result= tijera.comparar(papel);
		assertEquals("tijera le gana a papel", tijera.getDescripcionResultado());
	}
	
	@Test 
	void testTijeraGanaALagarto_comparar_DescripcionResultado() {
		int result= tijera.comparar(lagarto);
		assertEquals("tijera le gana a lagarto", tijera.getDescripcionResultado());
	}
	
	@Test 
	void testTijeraEmpataConTijera_comparar_DescripcionResultado() {
		int result= tijera.comparar(tijera);
		assertEquals("tijera empata tijera", tijera.getDescripcionResultado());
	}
	
	@Test 
	void testTijeraPierdeConPiedra_comparar_DescripcionResultado() {
		int result= tijera.comparar(piedra);
		assertEquals("tijera pierde con piedra", tijera.getDescripcionResultado());
	}
	
	@Test 
	void testTijeraPierdeConSpock_comparar_DescripcionResultado() {
		int result= tijera.comparar(spock);
		assertEquals("tijera pierde con spock", tijera.getDescripcionResultado());
	}
	
	
	// comparar Lagarto
	
	@Test 
	void testLagartoGanaAPapel_comparar_DescripcionResultado() {
		int result= lagarto.comparar(papel);
		assertEquals("lagarto le gana a papel", lagarto.getDescripcionResultado());
	}
	
	@Test 
	void testLagartoGanaASpock_comparar_DescripcionResultado() {
		int result= lagarto.comparar(spock);
		assertEquals("lagarto le gana a spock", lagarto.getDescripcionResultado());
	}
	
	@Test 
	void testLagartoEmpataConLagarto_comparar_DescripcionResultado() {
		int result= lagarto.comparar(lagarto);
		assertEquals("lagarto empata lagarto", lagarto.getDescripcionResultado());
	}
	
	@Test 
	void testLagartoPierdeConPiedra_comparar_DescripcionResultado() {
		int result= lagarto.comparar(piedra);
		assertEquals("lagarto pierde con piedra", lagarto.getDescripcionResultado());
	}
	
	@Test 
	void testLagartoPierdeConTijera_comparar_DescripcionResultado() {
		int result= lagarto.comparar(tijera);
		assertEquals("lagarto pierde con tijera", lagarto.getDescripcionResultado());
	}
	
	
	// comparar Spock
	
	@Test 
	void testSpockGanaATijera_comparar_DescripcionResultado() {
		int result= spock.comparar(tijera);
		assertEquals("spock le gana a tijera", spock.getDescripcionResultado());
	}
	
	@Test 
	void testSpockGanaAPiedra_comparar_DescripcionResultado() {
		int result= spock.comparar(piedra);
		assertEquals("spock le gana a piedra", spock.getDescripcionResultado());
	}
	
	@Test 
	void testSpockEmpataConSpock_comparar_DescripcionResultado() {
		int result= spock.comparar(spock);
		assertEquals("spock empata spock", spock.getDescripcionResultado());
	}
	
	@Test 
	void testSpockPierdeConPapel_comparar_DescripcionResultado() {
		int result= spock.comparar(papel);
		assertEquals("spock pierde con papel", spock.getDescripcionResultado());
	}
	
	@Test 
	void testSpockPierdeConLagarto_comparar_DescripcionResultado() {
		int result= spock.comparar(lagarto);
		assertEquals("spock pierde con lagarto", spock.getDescripcionResultado());
	}
	
	
	
	
	@Test
	void testGetInstance_tijera() {
		assertEquals("tijera", PiedraPapelTijeraLagartoSpockFactory.getInstance(PiedraPapelTijeraLagartoSpockFactory.TIJERA)
				.getNombre());

	}
	
	
	@Test
	void testGetInstance_piedra() {
		assertEquals("piedra", PiedraPapelTijeraLagartoSpockFactory.getInstance(PiedraPapelTijeraLagartoSpockFactory.PIEDRA)
				.getNombre());

	}
	
	
	@Test
	void testGetInstance_papel() {
		assertEquals("papel", PiedraPapelTijeraLagartoSpockFactory.getInstance(PiedraPapelTijeraLagartoSpockFactory.PAPEL)
				.getNombre());
	}
	

	@Test
	void testGetInstance_lagarto() {
		assertEquals("lagarto", PiedraPapelTijeraLagartoSpockFactory.getInstance(PiedraPapelTijeraLagartoSpockFactory.LAGARTO)
				.getNombre());
	}

	@Test
	void testGetInstance_spock() {
		assertEquals("spock", PiedraPapelTijeraLagartoSpockFactory.getInstance(PiedraPapelTijeraLagartoSpockFactory.SPOCK)
				.getNombre());
	}
	
	
}
