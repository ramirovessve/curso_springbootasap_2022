package ar.edu.asap.practica0.pptlspock.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.asap.practica0.pptlspock.Jugada;
import ar.edu.asap.practica0.pptlspock.Jugador;
import ar.edu.asap.practica0.pptlspock.Piedra;
import ar.edu.asap.practica0.pptlspock.Spock;



class JugadaTest {
	Jugada jugada=null;
	

	@BeforeEach
	void setUp() throws Exception {
		Jugador jug1 = new Jugador(1, "Gabriel", "papacho", new Piedra());
		Jugador jug2 = new Jugador(2, "compu", "pc", new Spock());
		jugada = new Jugada(1, new Date(), jug1, jug2);
	}

	@AfterEach
	void tearDown() throws Exception {
		jugada = null;
	}

	@Test
	void testgetDescripcionDelResultado() {
		assertEquals("piedra pierde con spock", jugada.getDescripcionDelresultado());
	}

}
