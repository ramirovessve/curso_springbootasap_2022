package ar.edu.asap.practica0.pptlspock;

public class Lagarto extends PiedraPapelTijeraLagartoSpockFactory {

	public Lagarto() {
		this("lagarto", PiedraPapelTijeraLagartoSpockFactory.LAGARTO);
	}
		
	
	public Lagarto(String nombre, int numero) {
		super(nombre, numero);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==PiedraPapelTijeraLagartoSpockFactory.LAGARTO;
	}

	@Override
	public int comparar(PiedraPapelTijeraLagartoSpockFactory pPPTLSFact) {
		int result=0;
		
		switch (pPPTLSFact.getNumero()) {
		case PiedraPapelTijeraLagartoSpockFactory.PAPEL:
			result=1;
			this.descripcionResultado = nombre + " le gana a " + pPPTLSFact.getNombre();		
		break;
		case PiedraPapelTijeraLagartoSpockFactory.SPOCK:
			result=1;
			this.descripcionResultado = nombre + " le gana a " + pPPTLSFact.getNombre();		
		break;
		case PiedraPapelTijeraLagartoSpockFactory.PIEDRA:
			result=-1;
			this.descripcionResultado = nombre + " pierde con " + pPPTLSFact.getNombre();		
		break;
		case PiedraPapelTijeraLagartoSpockFactory.TIJERA:
			result=-1;
			this.descripcionResultado = nombre + " pierde con " + pPPTLSFact.getNombre();		
		break;
		default:
			this.descripcionResultado = nombre + " empata " + pPPTLSFact.getNombre();
			break;
			
		}
	return result;
		
	}

}
