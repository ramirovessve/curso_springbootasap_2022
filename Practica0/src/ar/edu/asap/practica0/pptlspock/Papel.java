package ar.edu.asap.practica0.pptlspock;

public class Papel extends PiedraPapelTijeraLagartoSpockFactory {

	public Papel() {
		this("papel", PiedraPapelTijeraLagartoSpockFactory.PAPEL);
	}
	
	public Papel(String nombre, int numero) {
		super(nombre, numero);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum == PiedraPapelTijeraLagartoSpockFactory.PAPEL;
	}

	@Override
	public int comparar(PiedraPapelTijeraLagartoSpockFactory pPPTLSFact) {
		int result=0;
		
		switch (pPPTLSFact.getNumero()) {
		case PiedraPapelTijeraLagartoSpockFactory.PIEDRA:
			result=1;
			this.descripcionResultado = nombre + " le gana a " + pPPTLSFact.getNombre();		
		break;
		case PiedraPapelTijeraLagartoSpockFactory.SPOCK:
			result=1;
			this.descripcionResultado = nombre + " le gana a " + pPPTLSFact.getNombre();		
		break;
		case PiedraPapelTijeraLagartoSpockFactory.TIJERA:
			result=-1;
			this.descripcionResultado = nombre + " pierde con " + pPPTLSFact.getNombre();		
		break;
		case PiedraPapelTijeraLagartoSpockFactory.LAGARTO:
			result=-1;
			this.descripcionResultado = nombre + " pierde con " + pPPTLSFact.getNombre();		
		break;
		default:
			this.descripcionResultado = nombre + " empata " + pPPTLSFact.getNombre();
			break;
			
		}
		return result;
}
}
