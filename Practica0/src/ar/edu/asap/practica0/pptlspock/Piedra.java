package ar.edu.asap.practica0.pptlspock;

public class Piedra extends PiedraPapelTijeraLagartoSpockFactory {

	public Piedra() {
		this("piedra", PiedraPapelTijeraLagartoSpockFactory.PIEDRA);
	}
	
	
	public Piedra(String nombre, int numero) {
		super(nombre, numero);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum == PiedraPapelTijeraLagartoSpockFactory.PIEDRA;
	}

	@Override
	public int comparar(PiedraPapelTijeraLagartoSpockFactory pPPTLSFact) {
		int result=0;
		
		switch (pPPTLSFact.getNumero()) {
		case PiedraPapelTijeraLagartoSpockFactory.TIJERA:
			result=1;
			this.descripcionResultado = nombre + " le gana a " + pPPTLSFact.getNombre();		
		break;
		case PiedraPapelTijeraLagartoSpockFactory.LAGARTO:
			result=1;
			this.descripcionResultado = nombre + " le gana a " + pPPTLSFact.getNombre();		
		break;
		case PiedraPapelTijeraLagartoSpockFactory.PAPEL:
			result=-1;
			this.descripcionResultado = nombre + " pierde con " + pPPTLSFact.getNombre();		
		break;
		case PiedraPapelTijeraLagartoSpockFactory.SPOCK:
			result=-1;
			this.descripcionResultado = nombre + " pierde con " + pPPTLSFact.getNombre();		
		break;
		default:
			this.descripcionResultado = nombre + " empata " + pPPTLSFact.getNombre();
			break;
			
		}
		return result;
		
}
}
