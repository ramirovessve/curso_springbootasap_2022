package ar.edu.asap.practica0.pptlspock;

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraLagartoSpockFactory {
	public static final int PIEDRA =1;
	public static final int PAPEL =2;	
	public static final int TIJERA =3;
	public static final int LAGARTO =4;	
	public static final int SPOCK =5;
	
	protected String descripcionResultado;
	private static List<PiedraPapelTijeraLagartoSpockFactory> elementos;
	
	protected String nombre;
	protected int numero;
	
	// constructor
	
	public PiedraPapelTijeraLagartoSpockFactory(String nombre, int numero) {
		super();
		this.nombre = nombre;
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}
	
	public abstract boolean isMe(int pNum);
	public abstract int comparar(PiedraPapelTijeraLagartoSpockFactory pPPTLSFact);
	
	
	public static PiedraPapelTijeraLagartoSpockFactory getInstance(int pNum) {
		//el padre conoce a todos sus hijos
		elementos = new ArrayList<PiedraPapelTijeraLagartoSpockFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());
		elementos.add(new Lagarto());
		elementos.add(new Spock());
		
		
		for (PiedraPapelTijeraLagartoSpockFactory piedraPapelTijeraLagartoSpockFactory : elementos) {
			if (piedraPapelTijeraLagartoSpockFactory.isMe(pNum))
					return piedraPapelTijeraLagartoSpockFactory;
			
		}
			
		return null;
	}
	

}
